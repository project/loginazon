
$(document).ready(function(){

  function check_haspass() {
    if ($("input[@name='has_password']:checked").val() == '0'){
      $("#edit-pass").val("");
      $("#edit-pass").attr("disabled", "disabled");
      $("#edit-pass input").css("background-color","#cccccc !important");
    }
    else {
      $("#edit-pass").val("");
      $("#edit-pass").removeAttr("disabled", "disabled");
    }    
  } 
    
  // Set initial state
  check_haspass();
  
  // Respond to user action
  $("input[@name='has_password']").click(function(){
    check_haspass();
  });   
    
  
});